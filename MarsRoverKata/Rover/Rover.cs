﻿
using System.Collections.Generic;
using System.Diagnostics;

namespace MarsRoverKata.Rover
{
    public class Rover
    {
        public Point Coordinates { get; private set; }
        public CardinalDirection FacingDirection { get; private set; }

        public Rover(Point coordinates, CardinalDirection facingDirection)
        {
            Coordinates = coordinates;
            FacingDirection = facingDirection;
        }

        public Rover()
        {
            Coordinates = new Point();
            FacingDirection = new CardinalDirection();
        }



        public void ReceiveCommands(List<string> commandsArray)
        {
            foreach (var command in commandsArray)
            {
                var toExecuteCommand = new CommandFactory().CreateCommand(command);
                var afterMove = toExecuteCommand.Move(this);
                Coordinates = afterMove.Coordinates;
                FacingDirection = afterMove.FacingDirection;
            }
        }

        public Rover ForwardMove(Rover toMove)
        {
            return new Rover
            (
                coordinates: toMove.FacingDirection.MovePointForwardBasedOnDirection(toMove.Coordinates),
                facingDirection: toMove.FacingDirection
            );
        }
        private void BackwardMove()
        {
            Coordinates = FacingDirection.MovePointBackwardBasedOnDirection(Coordinates);
        }

        private void LeftMove()
        {
            FacingDirection = FacingDirection.GetLeftCardinalDirection();
        }

        private void RightMove()
        {
            FacingDirection = FacingDirection.GetRightCardinalDirection();
        }
    }

    public interface ICommand
    {
        Rover Move(Rover toMove);
        ICommand CreateCommand(string representation);
        bool RepresentationMatch(string representation);

    }

    public class Backward : ICommand
    {
        public string Representation { get; }

        public Backward()
        {
            Representation = $"{nameof(Backward)}";
        }
        public Rover Move(Rover toMove)
        {
            return new Rover
            (
                coordinates: toMove.FacingDirection.MovePointBackwardBasedOnDirection(toMove.Coordinates),
                facingDirection: toMove.FacingDirection
            );
        }

        public ICommand CreateCommand(string representation)
        {
            return representation == Representation
                ? new Backward()
                : new Forward().CreateCommand(representation);
        }

        public bool RepresentationMatch(string representation)
        {
            return Representation == representation;
        }
    }

    public class Forward : ICommand
    {
        public string Representation { get; }
        public Forward()
        {
            Representation = $"{nameof(Forward)}";
        }
        public Rover Move(Rover toMove)
        {
            return toMove.ForwardMove(toMove);
        }

        public ICommand CreateCommand(string representation)
        {
            return representation == Representation 
                ? new Forward() 
                : new Left().CreateCommand(representation);
        }

        public bool RepresentationMatch(string representation)
        {
            return Representation == representation;
        }
    }

    public class Left : ICommand
    {
        public string Representation { get; }

        public Left()
        {
            Representation = $"{nameof(Left)}";
        }
        public Rover Move(Rover toMove)
        {
            return new Rover
            (
                coordinates: toMove.Coordinates,
                facingDirection: toMove.FacingDirection.GetLeftCardinalDirection()
            );
        }

        public ICommand CreateCommand(string representation)
        {
            return representation == Representation
                ? new Left()
                : new Right().CreateCommand(representation);
        }

        public bool RepresentationMatch(string representation)
        {
            return Representation == representation;
        }
    }

    public class Right : ICommand
    {
        public string Representation { get; }
        public Right()
        {
            Representation = $"{nameof(Right)}";
        }
        public Rover Move(Rover toMove)
        {
            return new Rover
            (
                coordinates: toMove.Coordinates,
                facingDirection: toMove.FacingDirection.GetRightCardinalDirection()
            );
        }

        public ICommand CreateCommand(string representation)
        {
            return representation == Representation
                ? new Right()
                : new Backward().CreateCommand(representation);
        }

        public bool RepresentationMatch(string representation)
        {
            return Representation == representation;
        }
    }

    public class InvalidCommand : ICommand
    {
        public Rover Move(Rover toMove)
        {
            throw new System.NotImplementedException();
        }

        public ICommand CreateCommand(string representation)
        {
            throw new System.NotImplementedException();
        }

        public bool RepresentationMatch(string representation)
        {
            throw new System.NotImplementedException();
        }
    }

    public class CommandFactory
    {

        public ICommand CreateCommand(string representation)
        {
            var possibleCommands =  new List<ICommand>();
            possibleCommands.Add(new Forward());
            possibleCommands.Add(new Backward());
            possibleCommands.Add(new Left());
            possibleCommands.Add(new Right());


            foreach (var possibleCommand in possibleCommands)
            {
                if (possibleCommand.RepresentationMatch(representation))
                {
                    return possibleCommand;
                }
            }
            return new InvalidCommand();
        }

    }

}