﻿namespace MarsRoverKata.Rover
{
    public interface ICardinalDirection : IMovePoint
    {
        string Label { get; }
        ICardinalDirection GetRightNextCardinalDirection();
        ICardinalDirection GetLeftNextCardinalDirection();
    }

    public interface IMovePoint
    {
        Point MovePointForwardBasedOnDIrection(Point toMove);
        Point MovePointBackwardBasedOnDIrection(Point toMove);
    }

    public class North : ICardinalDirection
    {
        public string Label { get; }
        public North()
        {
            Label = $"{nameof(North)}";
        }

        public ICardinalDirection GetRightNextCardinalDirection()
        {
            return new East();
        }

        public ICardinalDirection GetLeftNextCardinalDirection()
        {
            return new West();
        }

        public Point MovePointForwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToYCoord(1);
        }

        public Point MovePointBackwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToYCoord(-1);
        }
    }
    public class East : ICardinalDirection
    {
        public string Label { get; }
        public East()
        {
            Label = $"{nameof(East)}";
        }

        public ICardinalDirection GetRightNextCardinalDirection()
        {
            return new South();
        }

        public ICardinalDirection GetLeftNextCardinalDirection()
        {
            return new North();
        }

        public Point MovePointForwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToXCoord(1);
        }

        public Point MovePointBackwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToXCoord(-1);
        }
    }
    public class South : ICardinalDirection
    {
        public string Label { get; }
        public South()
        {
            Label = $"{nameof(South)}";
        }

        public ICardinalDirection GetRightNextCardinalDirection()
        {
            return new West();
        }

        public ICardinalDirection GetLeftNextCardinalDirection()
        {
            return new East();
        }

        public Point MovePointForwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToYCoord(-1);
        }

        public Point MovePointBackwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToYCoord(1);
        }
    }
    
    public class West : ICardinalDirection
    {
        public string Label { get; }
        public West()
        {
            Label = $"{nameof(West)}";
        }

        public ICardinalDirection GetRightNextCardinalDirection()
        {
            return new North();
        }

        public ICardinalDirection GetLeftNextCardinalDirection()
        {
            return new South();
        }

        public Point MovePointForwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToXCoord(-1);
        }

        public Point MovePointBackwardBasedOnDIrection(Point toMove)
        {
            return toMove.AddToXCoord(1);
        }
    }
}