﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace MarsRoverKata
{
    [TestFixture]
    public class RoverTests
    {
        private Rover.Rover _sut;


        [SetUp]
        public void TestSetup()
        {
            _sut = new Rover.Rover();
        }

        [Test]
        public void DefaultRoverDisplaysCorrectInformationTest()
        {
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(0));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(0));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo("North"));
        }

        [Test]
        public void MoveForwardCommandWillMoveRoverCorrectlyTest()
        {
            var commandsArray = new List<string> {"Forward"};
            _sut.ReceiveCommands(commandsArray);
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(0));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(1));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo("North"));
        }

        [Test]
        public void MoveBackwardCommandWillMoveRoverCorrectlyTest()
        {
            var commandsArray = new List<string> { "Backward" };
            _sut.ReceiveCommands(commandsArray);
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(0));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(-1));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo("North"));
        }

        [Test]
        public void MoveRightCommandWillMoveRoverCorrectlyTest()
        {
            var commandsArray = new List<string> { "Right" };
            _sut.ReceiveCommands(commandsArray);
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(0));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(0));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo("East"));
        }

        [Test]
        public void MoveLeftCommandWillMoveRoverCorrectlyTest()
        {
            var commandsArray = new List<string> { "Left" };
            _sut.ReceiveCommands(commandsArray);
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(0));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(0));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo("West"));
        }


        [TestCase("Right", "Forward", 1, 0, "East")]
        [TestCase("Right", "Backward", -1, 0, "East")]
        [TestCase("Left", "Forward", -1, 0, "West")]
        [TestCase("Left", "Backward", 1, 0, "West")]
        public void TurnAndMoveCommandWillMoveRoverCorrectlyTest(string turn,string moveDir, int expectedX, int expectedY, string expectedCardinalPoint)
        {
            var commandsArray = new List<string> { turn, moveDir };
            _sut.ReceiveCommands(commandsArray);
            Assert.That(_sut.Coordinates.X.Value, Is.EqualTo(expectedX));
            Assert.That(_sut.Coordinates.Y.Value, Is.EqualTo(expectedY));
            Assert.That(_sut.FacingDirection.ActualDirection.Label, Is.EqualTo(expectedCardinalPoint));
        }




    }
}
