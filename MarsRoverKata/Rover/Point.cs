﻿namespace MarsRoverKata.Rover
{
    public class Point
    {
        public XCoord X { get; }
        public YCoord Y { get; }

        public Point(XCoord x,YCoord y)
        {
            X = x;
            Y = y;
        }
 
        public Point()
        {
            X = new XCoord();
            Y = new YCoord();
        }

        public Point AddToYCoord(int toAdd)
        {
            return new Point(X,Y.Add(toAdd));
        }
        public Point AddToXCoord(int toAdd)
        {
            return new Point(X.Add(toAdd), Y);
        }
    }

    public class XCoord
    {
        public int Value { get; }

        public XCoord()
        {
            Value = 0;
        }
        public XCoord(int value = 0)
        {
            Value = value;
        }
        public XCoord Add(int toAdd)
        {
            return new XCoord(Value + toAdd);
        }
    }

    public class YCoord
    {
        public int Value { get; }

        public YCoord()
        {
            Value = 0;
        }

        public YCoord(int value = 0)
        {
            Value = value;
        }

        public YCoord Add(int toAdd)
        {
            return new YCoord(Value+toAdd);
        }
    }






}