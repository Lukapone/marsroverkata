#include "catch.hpp"
#include <string>
#include <functional>
#include <type_traits>
#include <iostream>

class East;
using namespace std;

class Point {
private:
	const int xCoord{ 0 };
	const int yCoord{ 0 };
public:
	Point() = default;
	Point(int x, int y) : xCoord(x), yCoord(y) {
	}
	Point(const Point& x) = delete;
	Point(Point&& x) : xCoord(move(x.xCoord)), yCoord(move(x.yCoord)) {
	}
	const int XCoord () const
	{
		return xCoord;
	}
	const int YCoord() const {
		return yCoord;
	}
	Point AddToYCoord(const int toAdd) const {
		return Point(this->xCoord, this->yCoord + toAdd);
	}
	Point AddToXCoord(const int toAdd) const {
		return Point(this->xCoord + toAdd, this->yCoord);
	}

};
class IMovePoint {
public:
	virtual ~IMovePoint() {}
	virtual Point MovePointForwardBasedOnDIrection(const Point& toMove) const  = 0;
	virtual Point MovePointBackwardBasedOnDIrection(const Point& toMove) const = 0;
};


class ICardinalDirection : public IMovePoint {
public:
	virtual ~ICardinalDirection() {}
	virtual const string Label() const = 0;
	//virtual shared_ptr<ICardinalDirection> GetRightNextCardinalDirection() = 0;
	virtual unique_ptr<ICardinalDirection> GetRightNextCardinalDirection() const = 0;
	//virtual shared_ptr<ICardinalDirection> GetLeftNextCardinalDirection() = 0;
	virtual unique_ptr<ICardinalDirection> GetLeftNextCardinalDirection() const = 0;
	virtual unique_ptr<ICardinalDirection> GetNewSameCardinalDirection() const = 0;
};

#define quote(x) #x
class North : public ICardinalDirection {
private:
	const string label{ quote(North) };
public:
	North() = default;
	const string Label() const override;
	unique_ptr<ICardinalDirection> GetRightNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetLeftNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetNewSameCardinalDirection() const override;
	Point MovePointForwardBasedOnDIrection(const Point& toMove) const  override;
	Point MovePointBackwardBasedOnDIrection(const Point& toMove)const override;
};
class East : public ICardinalDirection {
private:
	const string label{ quote(East) };
public:
	East() = default;
	const string Label() const override;
	unique_ptr<ICardinalDirection> GetRightNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetLeftNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetNewSameCardinalDirection() const override;
	Point MovePointForwardBasedOnDIrection(const Point& toMove)const override;
	Point MovePointBackwardBasedOnDIrection(const Point& toMove)const override;
};
class South : public ICardinalDirection {
private:
	const string label{ quote(South) };
public:
	South() = default;
	const string Label() const override;
	unique_ptr<ICardinalDirection> GetRightNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetLeftNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetNewSameCardinalDirection() const override;
	Point MovePointForwardBasedOnDIrection(const Point& toMove)const override;
	Point MovePointBackwardBasedOnDIrection(const Point& toMove)const override;
};
class West : public ICardinalDirection {
private:
	const string label{ quote(West) };
public:
	West() = default;
	const string Label() const override;
	unique_ptr<ICardinalDirection> GetRightNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetLeftNextCardinalDirection() const override;
	unique_ptr<ICardinalDirection> GetNewSameCardinalDirection() const override;
	Point MovePointForwardBasedOnDIrection(const Point& toMove)const override;
	Point MovePointBackwardBasedOnDIrection(const Point& toMove)const override;
};

const string North::Label() const {
	return this->label;
}

unique_ptr<ICardinalDirection> North::GetRightNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<East>());
}
unique_ptr<ICardinalDirection> North::GetLeftNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<West>());
}

unique_ptr<ICardinalDirection> North::GetNewSameCardinalDirection() const
{
	return unique_ptr<ICardinalDirection>(make_unique<North>());
}

Point North::MovePointForwardBasedOnDIrection(const Point& toMove)const {
	return toMove.AddToYCoord(1);
}
Point North::MovePointBackwardBasedOnDIrection(const Point& toMove)const {
	return toMove.AddToYCoord(-1);
}

const string East::Label() const {
	return this->label;
}

unique_ptr<ICardinalDirection> East::GetRightNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<South>());
}
unique_ptr<ICardinalDirection> East::GetLeftNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<North>());
}
unique_ptr<ICardinalDirection> East::GetNewSameCardinalDirection() const
{
	return unique_ptr<ICardinalDirection>(make_unique<East>());
}
Point East::MovePointForwardBasedOnDIrection(const Point& toMove)const {
	return toMove.AddToXCoord(1);
}
Point East::MovePointBackwardBasedOnDIrection(const Point& toMove)const {
	return toMove.AddToXCoord(-1);
}
const string South::Label() const {
	return this->label;
}

unique_ptr<ICardinalDirection> South::GetRightNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<West>());
}
unique_ptr<ICardinalDirection> South::GetLeftNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<East>());
}
unique_ptr<ICardinalDirection> South::GetNewSameCardinalDirection() const
{
	return unique_ptr<ICardinalDirection>(make_unique<South>());
}
Point South::MovePointForwardBasedOnDIrection(const Point& toMove) const {
	return toMove.AddToYCoord(-1);
}
Point South::MovePointBackwardBasedOnDIrection(const Point& toMove) const {
	return toMove.AddToYCoord(1);
}
const string West::Label() const {
	return this->label;
}

unique_ptr<ICardinalDirection> West::GetRightNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<North>());
}
unique_ptr<ICardinalDirection> West::GetLeftNextCardinalDirection() const {
	return unique_ptr<ICardinalDirection>(make_unique<South>());
}
unique_ptr<ICardinalDirection> West::GetNewSameCardinalDirection() const
{
	return unique_ptr<ICardinalDirection>(make_unique<West>());
}
Point West::MovePointForwardBasedOnDIrection(const Point& toMove) const {
	return toMove.AddToXCoord(-1);
}
Point West::MovePointBackwardBasedOnDIrection(const Point& toMove)const {
	return toMove.AddToXCoord(1);
}

class CardinalDirection {
private:
     unique_ptr<ICardinalDirection> actualDirection;
public:
	CardinalDirection() : actualDirection(make_unique<North>()) {};
	//CardinalDirection(unique_ptr<ICardinalDirection> act) : actualDirection(std::move(act)) {};
	CardinalDirection(unique_ptr<ICardinalDirection>&& act) : actualDirection(std::move(act)) {};
	CardinalDirection(CardinalDirection&& x): actualDirection(move(x.actualDirection)){}

	//this will require to remove const as we are changing the state by moving
	//CardinalDirection(CardinalDirection&& toMove) : actualDirection(std::move(toMove.actualDirection)) {};
	/*CardinalDirection(ICardinalDirection* initDirection): actualDirection(initDirection) {
	}*/
    /*const shared_ptr<ICardinalDirection> ActualDirection() const {
		return actualDirection;
	}*/
	CardinalDirection(CardinalDirection& x) = delete;
	const ICardinalDirection* ActualDirection() const {
		return actualDirection.get();
	}
	const ICardinalDirection& ActualDirection() {
		return *(actualDirection);
	}

	const unique_ptr<ICardinalDirection> const& ActualDirectionD() const {
		return actualDirection;
	}
	unique_ptr<ICardinalDirection> GetRightCardinalDirection() const
	{
		return actualDirection.get()->GetRightNextCardinalDirection();
	}
	unique_ptr<ICardinalDirection> GetLeftCardinalDirection() const
	{
		return actualDirection.get()->GetLeftNextCardinalDirection();
	}

	unique_ptr<ICardinalDirection> GetSameCardinalDirection() const
	{
		return actualDirection.get()->GetNewSameCardinalDirection();
	}
	Point MovePointForwardBasedOnDirection(const Point& toMove) const
	{
		return actualDirection.get()->MovePointForwardBasedOnDIrection(toMove);
	}
	Point MovePointBackwardBasedOnDirection(const Point& toMove) const
	{
		return actualDirection.get()->MovePointBackwardBasedOnDIrection(toMove);
	}

};

class Rover {
private:
	Point coordinates;
	CardinalDirection facingDirection;
public:
	Rover() = default;
	~Rover() = default;
	Rover(Point&& initPoint, CardinalDirection&& initDir) : coordinates(move(initPoint)), facingDirection(move(initDir)) {
	}
	//Rover(Rover&& o) noexcept : Coordinates(std::move(o.Coordinates)), FacingDirection(o.FacingDirection) { }
	Rover(Rover&& x) : coordinates(move(x.coordinates)), facingDirection(move(x.facingDirection)) {
	}
	Rover(const Rover & toConst) = delete;
	//Rover ReceiveCommands(vector<string> commands);
	//void Forward();
	const Point& Coordinates() const
	{
		return coordinates;
	}
	const CardinalDirection& FacingDirection() const {
		return facingDirection;
	}

};

class ICommand {
public:
	virtual ~ICommand() {}
	virtual shared_ptr<ICommand> CreateCommand(string representation) = 0;
	virtual bool RepresentationMatch(string representation) = 0;
	//virtual Rover Move(Rover&& toMove) = 0;
	//virtual Rover Move(Rover& toMove) = 0;
	virtual Rover Move(const Rover& toMove) = 0;
};
class CommandFactory {
public:
	shared_ptr<ICommand> CreateCommandF(string representation);
};
Rover ReceiveCommands(vector<string> commands, Rover toMove);
//Rover MoveRoverWithCommand(Rover toMove, shared_ptr<ICommand> moveCmd);

Rover ReceiveCommands(vector<string> commands, Rover toMove) {
	auto vecRovers = vector<Rover>();
	vecRovers.push_back(move(toMove));
	for (const auto& comand : commands) {
		auto afterMove = CommandFactory().CreateCommandF(comand).get()->Move(
			vecRovers.back()
		);
		//vecRovers.pop_back();
		vecRovers.push_back(move(afterMove));
	}
	return move(vecRovers.back());
}
//Rover MoveRoverWithCommand(Rover toMove, shared_ptr<ICommand> moveCmd) {
//	return move(moveCmd.get()->Move(
//		move(Rover(
//			Point(toMove.Coordinates.XCoord(), toMove.Coordinates.YCoord()),
//			CardinalDirection(move(toMove.FacingDirection))
//		))
//	));
//}


class Backward : public ICommand
{
private:
	const string representation;
public:
	Backward() :representation(quote(Backward)) {};
	//Rover Move(Rover&& toMove) override;
	//Rover Move(Rover& toMove) override;
	Rover Move(const Rover& toMove) override;
	shared_ptr<ICommand> CreateCommand(string representation) override;
	bool RepresentationMatch(string representation) override;
};
class Forward : public ICommand
{
private:
	const string representation;
public:
	Forward() :representation(quote(Forward)) {};
	//Rover Move(Rover&& toMove)override;
	//Rover Move(Rover& toMove) override;
	Rover Move(const Rover& toMove) override;
	shared_ptr<ICommand> CreateCommand(string representation)override;
	bool RepresentationMatch(string representation)override;
};
class Left : public ICommand
{
private:
	const string representation;
public:
	Left() :representation(quote(Left)) {};
	//Rover Move(Rover&& toMove)override;
	//Rover Move(Rover& toMove) override;
	Rover Move(const Rover& toMove) override;
	shared_ptr<ICommand> CreateCommand(string representation)override;
	bool RepresentationMatch(string representation)override;
};
class Right : public ICommand
{
private:
	const string representation;
public:
	Right() :representation(quote(Right)) {};
	/*Rover Move(Rover&& toMove)override;*/
	//Rover Move(Rover& toMove) override;
	Rover Move(const Rover& toMove) override;
	shared_ptr<ICommand> CreateCommand(string representation)override;
	bool RepresentationMatch(string representation)override;
};


//Rover Backward::Move(Rover&& toMove) {
//	return Rover(
//		toMove.FacingDirection.MovePointBackwardBasedOnDirection(move(toMove.Coordinates())),
//		move(toMove.FacingDirection().GetSameCardinalDirection())
//	);
//}

//Rover Backward::Move(Rover & toMove)
//{
//	return Rover(
//		Point(toMove.FacingDirection().MovePointBackwardBasedOnDirection(toMove.Coordinates())),
//		CardinalDirection(toMove.FacingDirection().GetSameCardinalDirection())
//	);
//}

Rover Backward::Move(const Rover & toMove)
{
	return Rover(
		Point(toMove.FacingDirection().MovePointBackwardBasedOnDirection(toMove.Coordinates())),
		CardinalDirection(toMove.FacingDirection().GetSameCardinalDirection())
	);
}

shared_ptr<ICommand> Backward::CreateCommand(string representation) {
	return this->representation == representation
		? make_shared<Backward>()
		: make_shared<Forward>().get()->CreateCommand(representation);//new Forward().CreateCommand(representation);
	/*if (this->representation == representation) {
		return make_shared<Backward>();
	}
	return make_shared<Forward>().get()->CreateCommand(representation);*/
}

bool Backward::RepresentationMatch(string representation)
{
	return this->representation == representation;
}

//Rover Forward::Move(Rover&& toMove)
//{
//	return Rover(
//		toMove.FacingDirection.MovePointForwardBasedOnDirection(move(toMove.Coordinates())),
//		move(toMove.FacingDirection().GetSameCardinalDirection())
//	);
//}

//Rover Forward::Move(Rover & toMove)
//{
//	return Rover(
//		Point(toMove.FacingDirection().MovePointForwardBasedOnDirection(toMove.Coordinates())),
//		CardinalDirection(toMove.FacingDirection().GetSameCardinalDirection())
//	);
//}

Rover Forward::Move(const Rover & toMove)
{
	return Rover(
		Point(toMove.FacingDirection().MovePointForwardBasedOnDirection(toMove.Coordinates())),
		CardinalDirection(toMove.FacingDirection().GetSameCardinalDirection())
	);
}

shared_ptr<ICommand> Forward::CreateCommand(string representation)
{
	return this->representation == representation
		? make_shared<Forward>()
		: make_shared<Left>().get()->CreateCommand(representation);
}

bool Forward::RepresentationMatch(string representation)
{
	return this->representation == representation;
}

//Rover Left::Move(Rover&& toMove)
//{
//	return Rover(
//		move(toMove.Coordinates()),
//		move(toMove.FacingDirection().GetLeftCardinalDirection())
//	);
//}

//Rover Left::Move(Rover & toMove)
//{
//	return Rover(
//		Point(toMove.Coordinates().XCoord(), toMove.Coordinates().YCoord()),
//		CardinalDirection(toMove.FacingDirection().GetLeftCardinalDirection())
//	);
//}

Rover Left::Move(const Rover & toMove)
{
	return Rover(
		Point(toMove.Coordinates().XCoord(), toMove.Coordinates().YCoord()),
		CardinalDirection(toMove.FacingDirection().GetLeftCardinalDirection())
	);
}

shared_ptr<ICommand> Left::CreateCommand(string representation)
{
	return this->representation == representation
		? make_shared<Left>()
		: make_shared<Right>().get()->CreateCommand(representation);
}

bool Left::RepresentationMatch(string representation)
{
	return this->representation == representation;
}

//Rover Right::Move(Rover&& toMove)
//{
//	return Rover(
//		move(toMove.Coordinates),
//		move(toMove.FacingDirection.GetRightCardinalDirection())
//	);
//}

//Rover Right::Move(Rover & toMove)
//{
//	return Rover(
//		Point(toMove.Coordinates().XCoord(), toMove.Coordinates().YCoord()),
//		CardinalDirection(toMove.FacingDirection().GetRightCardinalDirection())
//	);
//}

Rover Right::Move(const Rover & toMove)
{
	return Rover(
		Point(toMove.Coordinates().XCoord(), toMove.Coordinates().YCoord()),
		CardinalDirection(toMove.FacingDirection().GetRightCardinalDirection())
	);
}


shared_ptr<ICommand> Right::CreateCommand(string representation)
{
	return this->representation == representation
		? make_shared<Right>()
		: make_shared<Backward>().get()->CreateCommand(representation);
}

bool Right::RepresentationMatch(string representation)
{
	return this->representation == representation;
}


shared_ptr<ICommand> CommandFactory::CreateCommandF(string representation) {
	auto possibleCommands = vector<shared_ptr<ICommand>>();
	possibleCommands.push_back(make_shared<Forward>());
	possibleCommands.push_back(make_shared<Backward>());
	possibleCommands.push_back(make_shared<Left>());
	possibleCommands.push_back(make_shared<Right>());

	for (const auto& posCommand : possibleCommands) {
		if (posCommand.get()->RepresentationMatch(representation)) {
			return posCommand;
		}
	}

	return nullptr;
}


TEST_CASE("Default constructed rover") {

	 Rover _sut;//default constructor
	
	 //auto _sut = Rover();//copy constructor

	SECTION("displays correct default information") {

		std::cout << std::boolalpha << "Rover is_default_constructible: " << std::is_default_constructible<Rover>::value << std::endl;
		std::cout << std::boolalpha << "Rover is_copy_constructible: " << std::is_copy_constructible<Rover>::value << std::endl;
		std::cout << std::boolalpha << "Rover is_copy_assignable: " << std::is_copy_assignable<Rover>::value << std::endl;
		std::cout << std::boolalpha << "Rover is_move_constructible: " << std::is_move_constructible<Rover>::value << std::endl;
		std::cout << std::boolalpha << "Rover is_move_assignable: " << std::is_move_assignable<Rover>::value << std::endl;
		std::cout << std::boolalpha << "Rover is_destructible: " << std::is_destructible<Rover>::value << std::endl;

		std::cout << std::boolalpha << "CardinalDirection is_default_constructible: " << std::is_default_constructible<CardinalDirection>::value << std::endl;
		std::cout << std::boolalpha << "CardinalDirection is_copy_constructible: " << std::is_copy_constructible<CardinalDirection>::value << std::endl;
		std::cout << std::boolalpha << "CardinalDirection is_copy_assignable: " << std::is_copy_assignable<CardinalDirection>::value << std::endl;
		std::cout << std::boolalpha << "CardinalDirection is_move_constructible: " << std::is_move_constructible<CardinalDirection>::value << std::endl;
		std::cout << std::boolalpha << "CardinalDirection is_move_assignable: " << std::is_move_assignable<CardinalDirection>::value << std::endl;
		std::cout << std::boolalpha << "CardinalDirection is_destructible: " << std::is_destructible<CardinalDirection>::value << std::endl;

		REQUIRE(_sut.Coordinates().XCoord() == 0);
		REQUIRE(_sut.Coordinates().YCoord() == 0);
		REQUIRE(_sut.FacingDirection().ActualDirectionD().get()->Label() == "North");
	}

	SECTION("Move rover forward will move it correctly") {
		auto commandsArray = vector<string>{ "Forward" };
		auto _sutAfter = ReceiveCommands(commandsArray, move(_sut));
		REQUIRE(_sutAfter.Coordinates().XCoord() == 0);
		REQUIRE(_sutAfter.Coordinates().YCoord() == 1);
		REQUIRE(_sutAfter.FacingDirection().ActualDirectionD().get()->Label() == "North");
	}

	SECTION("Move rover Backward will move it correctly") {
		auto commandsArray = vector<string>{ "Backward" };
		auto _sutAfter = ReceiveCommands(commandsArray, move(_sut));
		REQUIRE(_sutAfter.Coordinates().XCoord() == 0);
		REQUIRE(_sutAfter.Coordinates().YCoord() == -1);
		REQUIRE(_sutAfter.FacingDirection().ActualDirectionD().get()->Label() == "North");
	}

	SECTION("Move rover Right will move it correctly") {
		auto commandsArray = vector<string>{ "Right" };
		auto _sutAfter = ReceiveCommands(commandsArray, move(_sut));
		REQUIRE(_sutAfter.Coordinates().XCoord() == 0);
		REQUIRE(_sutAfter.Coordinates().YCoord() == 0);
		REQUIRE(_sutAfter.FacingDirection().ActualDirectionD().get()->Label() == "East");
	}

	SECTION("Move rover Left will move it correctly") {
		auto commandsArray = vector<string>{ "Left" };
		auto _sutAfter = ReceiveCommands(commandsArray, move(_sut));
		REQUIRE(_sutAfter.Coordinates().XCoord() == 0);
		REQUIRE(_sutAfter.Coordinates().YCoord() == 0);
		REQUIRE(_sutAfter.FacingDirection().ActualDirectionD().get()->Label() == "West");
	}


	SECTION("Move rover Right Forward will move it correctly") {
		auto commandsArray = vector<string>{ "Right", "Forward", "Left", "Backward" };
		auto _sutAfter = ReceiveCommands(commandsArray, move(_sut));
		REQUIRE(_sutAfter.Coordinates().XCoord() == 1);
		REQUIRE(_sutAfter.Coordinates().YCoord() == -1);
		REQUIRE(_sutAfter.FacingDirection().ActualDirectionD().get()->Label() == "North");
	}

};




// Compile: see 020-TestCase-1.cpp

// Expected compact output (all assertions):
//
// prompt> 020-TestCase --reporter compact --success
// 020-TestCase-2.cpp:13: failed: Factorial(0) == 1 for: 0 == 1
// 020-TestCase-2.cpp:17: passed: Factorial(1) == 1 for: 1 == 1
// 020-TestCase-2.cpp:18: passed: Factorial(2) == 2 for: 2 == 2
// 020-TestCase-2.cpp:19: passed: Factorial(3) == 6 for: 6 == 6
// 020-TestCase-2.cpp:20: passed: Factorial(10) == 3628800 for: 3628800 (0x375f00) == 3628800 (0x375f00)
// Failed 1 test case, failed 1 assertion.


