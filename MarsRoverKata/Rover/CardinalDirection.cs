﻿namespace MarsRoverKata.Rover
{
    public class CardinalDirection
    {
        public ICardinalDirection ActualDirection { get; }

        public CardinalDirection(ICardinalDirection actualDirection)
        {
            ActualDirection = actualDirection;
        }

        public CardinalDirection()
        {
            ActualDirection = new North();
        }

        public CardinalDirection GetRightCardinalDirection()
        {
            return new CardinalDirection(ActualDirection.GetRightNextCardinalDirection());
        }
        public CardinalDirection GetLeftCardinalDirection()
        {
            return new CardinalDirection(ActualDirection.GetLeftNextCardinalDirection());
        }

        public Point MovePointForwardBasedOnDirection(Point toMove)
        {
            return ActualDirection.MovePointForwardBasedOnDIrection(toMove);
        }
        public Point MovePointBackwardBasedOnDirection(Point toMove)
        {
            return ActualDirection.MovePointBackwardBasedOnDIrection(toMove);
        }

    }

    //public class RoverForwardMover
    //{

    //    public Rover MoveForward(Rover toMove)
    //    {

    //    }

    //}


}